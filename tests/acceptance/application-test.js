import { test } from 'qunit';
import moduleForAcceptance from 'mirage-test/tests/helpers/module-for-acceptance';
import fakeJson from 'mirage-test/tests/helpers/fake-json';

moduleForAcceptance('Acceptance | application');

test('visiting /', function(assert) {
  visit('/');

  console.log(fakeJson());

  andThen(function() {
    assert.equal(currentURL(), '/');
  });
});
