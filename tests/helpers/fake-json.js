import Ember from 'ember';
import dummyJson from 'npm:dummy-json';

export default function fakeJson(attrs) {
  console.log(dummyJson.parse());
  return {
    a: 1
  };
}
